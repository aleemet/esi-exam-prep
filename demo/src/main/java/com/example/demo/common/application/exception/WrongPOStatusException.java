package com.example.demo.common.application.exception;

public class WrongPOStatusException extends Exception {
    public WrongPOStatusException(String s) {
        super(s);
    }
}
