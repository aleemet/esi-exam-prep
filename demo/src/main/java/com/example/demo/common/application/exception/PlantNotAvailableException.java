package com.example.demo.common.application.exception;

public class PlantNotAvailableException extends Exception {
    public PlantNotAvailableException(String s) {
        super(s);
    }
}
