package com.example.demo.sales.rest;

import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.POStatus;
import com.example.demo.sales.domain.PurchaseOrder;
import com.example.demo.sales.domain.PurchaseOrderRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class PurchaseOrderRentalPeriodChange {


    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired @Qualifier("_halObjectMapper")
    ObjectMapper mapper;

    @Before // Use `Before` from Cucumber library
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @After  // Use `After` from Cucumber library
    public void tearOff() {
        purchaseOrderRepository.deleteAll();
        plantReservationRepository.deleteAll();
        plantInventoryItemRepository.deleteAll();
        plantInventoryEntryRepository.deleteAll();
    }

    @And("^the following purchase orders$")
    public void theFollowingPurchaseOrders(DataTable table) throws Throwable {
        for (Map<String, String> row: table.asMaps(String.class, String.class)) {
            BusinessPeriod period = BusinessPeriod.of(LocalDate.parse(row.get("startDate")), LocalDate.parse(row.get("endDate")));
            PlantInventoryEntry entry = plantInventoryEntryRepository.findById(Long.parseLong(row.get("plant"))).orElse(null);

            PlantReservation reservation = new PlantReservation();
            reservation.setId(Long.parseLong(row.get("id")));
            reservation.setSchedule(period);
            reservation.setPlant(plantInventoryItemRepository.findAllByPlantInfo(entry).get(0));
            plantReservationRepository.save(reservation);

            PurchaseOrder po = PurchaseOrder.of(entry, period);
            po.setId(Long.parseLong(row.get("id")));
            po.addReservation(reservation);
            po.setStatus(POStatus.valueOf(row.get("status")));

            po = purchaseOrderRepository.save(po);
        }
    }

    @When("^changing the period start to \"([^\"]*)\" and end to \"([^\"]*)\" of purchase order \"([^\"]*)\"$")
    public void changingThePeriodStartToAndEndToOfPurchaseOrder(String arg0, String arg1, String arg2) throws Exception {
        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.parse(arg0), LocalDate.parse(arg1)));

        purchaseOrderRepository.findAll().forEach(PO -> System.out.println("\n purchase orders: " + PO.getId()));

        MvcResult result = mockMvc.perform(
                patch("/api/sales/orders/{id}", arg2)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(order)))
                .andReturn();

        //System.out.println("\n" + result.getResponse().getContentAsString()); // See the returned message  (data or error msg)
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        order = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);

        assertThat(order.getRentalPeriod().getStartDate()).isEqualTo(arg0);
        assertThat(order.getRentalPeriod().getEndDate()).isEqualTo(arg1);
    }

    @Then("^the reservation of purchase order \"([^\"]*)\" has the same item \"([^\"]*)\" and period is now from \"([^\"]*)\" and to \"([^\"]*)\"$")
    public void theReservationOfPurchaseOrderHasTheSameItemAndPeriodIsNowFromAndTo(String arg0, String arg1, String arg2, String arg3) throws Throwable {
        PurchaseOrder order = purchaseOrderRepository.findById(Long.parseLong(arg0)).orElse(null);
        PlantReservation reservation = order.getReservations().get(0);
        assertThat(reservation.getPlant().getId()).isEqualTo(Integer.parseInt(arg1));
        assertThat(reservation.getSchedule().getStartDate()).isEqualTo(arg2);
        assertThat(reservation.getSchedule().getEndDate()).isEqualTo(arg3);
    }

    @Then("^the reservation of purchase order \"([^\"]*)\" has item other than \"([^\"]*)\" and period is now from \"([^\"]*)\" and to \"([^\"]*)\"$")
    public void theReservationOfPurchaseOrderHasItemOtherThanAndPeriodIsNowFromAndTo(String arg0, String arg1, String arg2, String arg3) throws Throwable {
        PurchaseOrder order = purchaseOrderRepository.findById(Long.parseLong(arg0)).orElse(null);
        PlantReservation reservation = order.getReservations().get(0);
        assertThat(reservation.getPlant().getId()).isNotEqualTo(Integer.parseInt(arg1));
        assertThat(reservation.getSchedule().getStartDate()).isEqualTo(arg2);
        assertThat(reservation.getSchedule().getEndDate()).isEqualTo(arg3);
    }

    @When("^changing the period start to \"([^\"]*)\" and end to \"([^\"]*)\" of purchase order \"([^\"]*)\" fails$")
    public void changingThePeriodStartToAndEndToOfPurchaseOrderFails(String arg0, String arg1, String arg2) throws Throwable {
        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.parse(arg0), LocalDate.parse(arg1)));

        //purchaseOrderRepository.findAll().forEach(PO -> System.out.println("\n purchase orders: " + PO.getId())); // Print out all Purchase orders

        MvcResult result = mockMvc.perform(
                patch("/api/sales/orders/{id}", arg2)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(order)))
                .andReturn();

        //System.out.println("\n" + result.getResponse().getContentAsString());  // See the returned message (data or error msg)
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
        assertThat(result.getResponse().getContentAsString()).isEqualTo("No available plant for given period");
    }

    @Then("^the reservation of purchase order \"([^\"]*)\" has the same data as before: item \"([^\"]*)\" and same period from \"([^\"]*)\" and to \"([^\"]*)\"$")
    public void theReservationOfPurchaseOrderHasTheSameDataAsBeforeItemAndSamePeriodFromAndTo(String arg0, String arg1, String arg2, String arg3) throws Throwable {
        PurchaseOrder order = purchaseOrderRepository.findById(Long.parseLong(arg0)).orElse(null);
        PlantReservation reservation = order.getReservations().get(0);
        assertThat(reservation.getPlant().getId()).isEqualTo(Integer.parseInt(arg1));
        assertThat(reservation.getSchedule().getStartDate()).isEqualTo(arg2);
        assertThat(reservation.getSchedule().getEndDate()).isEqualTo(arg3);
    }
}
