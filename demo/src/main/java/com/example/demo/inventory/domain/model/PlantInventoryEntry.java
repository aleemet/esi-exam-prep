package com.example.demo.inventory.domain.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Value
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
@AllArgsConstructor(staticName="of")
public class PlantInventoryEntry {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;
    String description;
    @Column(precision=8, scale=2)
    BigDecimal price;
}
