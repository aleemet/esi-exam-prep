# Created by aleemet at 01/06/2019
Feature: Changing the rental period of Purchase order
  # Note that the ID's are sequential, meaning that regardless of table
  # The items added to the table have their ID as the n'th number, n
  # showing how many items have there been inserted in the database
  # by that time (i.e 5 means the item was the 5th entry inserted to the database)

  #### IMPORTANT: make sure the run configuration is CUCUMBER JAVA (NOT cucumber js)

  Background: Plant catalog
    Given the following plant catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
    And the following inventory
      | id | plantInfo | serialNumber | equipmentCondition |
      |  1 |     1     | exc-mn1.5-01 | SERVICEABLE        |
      |  2 |     1     | exc-mn1.5-01 | SERVICEABLE        |
      |  3 |     2     | exc-mn3.0-01 | SERVICEABLE        |
    And the following purchase orders
      | id | status | startDate | endDate   | plant | total |
      |  1 |  OPEN  | 2019-09-22| 2019-09-24|   1   | 300   |
      |  2 |  OPEN  | 2019-10-01| 2019-10-10|   1   | 1500   |
      |  3 |  OPEN  | 2019-09-22| 2019-09-24|   2   | 400   |
      |  4 |  OPEN  | 2019-10-01| 2019-10-10|   2   | 2000   |

  Scenario: Changing the period
    When changing the period start to "2019-09-23" and end to "2019-09-28" of purchase order "1"
    Then the reservation of purchase order "1" has the same item "1" and period is now from "2019-09-23" and to "2019-09-28"

  Scenario: Changing the period when same item not available
    When changing the period start to "2019-10-05" and end to "2019-10-08" of purchase order "1"
    Then the reservation of purchase order "1" has item other than "1" and period is now from "2019-10-05" and to "2019-10-08"

  Scenario: Changing the period when no items available
    When changing the period start to "2019-10-05" and end to "2019-10-08" of purchase order "3" fails
    Then the reservation of purchase order "3" has the same data as before: item "3" and same period from "2019-09-22" and to "2019-09-24"