package com.example.demo.common.application.exception;

public class WrongDateException extends Exception {
    public WrongDateException(String string) {
        super(string);
    }
}
