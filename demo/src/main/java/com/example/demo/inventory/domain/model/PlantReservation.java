package com.example.demo.inventory.domain.model;

import com.example.demo.common.domain.BusinessPeriod;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class PlantReservation {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Embedded
    BusinessPeriod schedule;

    @ManyToOne
    PlantInventoryItem plant;
}
