package com.example.demo.sales.application.service;

import com.example.demo.common.application.exception.PONotFoundException;
import com.example.demo.common.application.exception.PlantNotAvailableException;
import com.example.demo.common.application.exception.WrongDateException;
import com.example.demo.common.application.exception.WrongPOStatusException;
import com.example.demo.common.application.service.BusinessPeriodValidator;
import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.POStatus;
import com.example.demo.sales.domain.PurchaseOrder;
import com.example.demo.sales.domain.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.time.LocalDate;
import java.util.List;

@Service
public class SalesService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PurchaseOrderRepository poRepository;

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    public PurchaseOrderDTO findPO(Long id) {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO createPO(PurchaseOrderDTO poDTO) throws Exception {

        BusinessPeriod period = BusinessPeriod.of(
                         poDTO.getRentalPeriod().getStartDate(),
                         poDTO.getRentalPeriod().getEndDate());

        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new Exception("Invalid PO Period");

        if(poDTO.getPlant() == null)
            throw new Exception("Invalid Input Plant");

        PlantInventoryEntry plant = plantInventoryEntryRepository.findById(poDTO.getPlant().get_id()).orElse(null);

        if(plant == null)
            throw new Exception("Plant NOT Found");

        PurchaseOrder po = PurchaseOrder.of(plant, period);
        po.setId(poRepository.count() + 1);
        poRepository.save(po);

        List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                po.getPlant().getId(),
                po.getRentalPeriod().getStartDate(),
                po.getRentalPeriod().getEndDate());

        if(availableItems.size() == 0) {
            po.setStatus(POStatus.REJECTED);
            throw new Exception("No available items");
        }

        PlantReservation reservation = new PlantReservation();
        reservation.setSchedule(po.getRentalPeriod());
        reservation.setPlant(availableItems.get(0));
        reservation.setId(plantReservationRepository.count() + 1);

        plantReservationRepository.save(reservation);
        po.getReservations().add(reservation);

        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);

    }

    public PurchaseOrderDTO acceptPO(Long id) throws Exception {
        PurchaseOrder po = getPO(id);
        po.setStatus(POStatus.OPEN);
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO rejectPO(Long id) throws Exception {
        PurchaseOrder po = getPO(id);
        while (!po.getReservations().isEmpty()) {
            plantReservationRepository.delete(po.getReservations().remove(0));
        }
        po.setStatus(POStatus.CLOSED);
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    private PurchaseOrder getPO(Long id) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if(po == null)
            throw new Exception("PO Not Found");
        if(po.getStatus() != POStatus.PENDING)
            throw new Exception("PO cannot be accepted/rejected due to it is not Pending");
        return po;
    }

    public PurchaseOrderDTO changePOPeriod(Long id, PurchaseOrderDTO partialPODTO) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);

        if (po == null) {
            throw new PONotFoundException(id);
        }
        if (po.getStatus() != POStatus.OPEN) {
            throw new WrongPOStatusException("Only ACCEPTED Purchase Orders can have their rental period changed");
        }
        if (po.getRentalPeriod().getStartDate().isBefore(LocalDate.now().plusDays(1))) {
            throw new WrongDateException("Rental starts today or has already started");
        }

        BusinessPeriod period = BusinessPeriod.of(
                partialPODTO.getRentalPeriod().getStartDate(),
                partialPODTO.getRentalPeriod().getEndDate());

        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors()) {
            throw new WrongDateException("Given rental period is invalid");
        }

        for (PlantReservation reservation: po.getReservations()) {
            PlantInventoryItem item = reservation.getPlant();
            reservation.setPlant(null);
            plantReservationRepository.save(reservation);

            List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                    po.getPlant().getId(),
                    period.getStartDate(),
                    period.getEndDate());

            if(availableItems.size() == 0) {
                reservation.setPlant(item);
                plantReservationRepository.save(reservation);
                throw new PlantNotAvailableException("No available plant for given period");
            }
            else if (availableItems.contains(item)) {
                reservation.setPlant(item);
                reservation.setSchedule(period);
                plantReservationRepository.save(reservation);
            }
            else {
                reservation.setPlant(availableItems.get(0));
                reservation.setSchedule(period);
                plantReservationRepository.save(reservation);
            }
        }

        po.setRentalPeriod(period);
        poRepository.save(po);

        return purchaseOrderAssembler.toResource(po);
    }
}
