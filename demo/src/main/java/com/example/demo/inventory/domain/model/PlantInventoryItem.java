package com.example.demo.inventory.domain.model;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Value
@Entity
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
@AllArgsConstructor(staticName="of")
public class PlantInventoryItem {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String serialNumber;

    @Enumerated(EnumType.STRING)
    EquipmentCondition equipmentCondition;

    @ManyToOne
    PlantInventoryEntry plantInfo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlantInventoryItem that = (PlantInventoryItem) o;
        return id.equals(that.id);
    }
}
