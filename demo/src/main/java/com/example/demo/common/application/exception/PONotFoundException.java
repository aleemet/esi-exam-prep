package com.example.demo.common.application.exception;

public class PONotFoundException extends Exception {
    public PONotFoundException(Long id) {
        super(String.format("Po with ID %d not found", id));
    }
}
